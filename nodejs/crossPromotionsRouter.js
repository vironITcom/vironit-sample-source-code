/**
 * Copyright (c) 2017, VironIT and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * This code is part of VironIT software; Part of Relief app.
 *
 * Node example app -  part of main app 
 *
 * @author   VironIT
 * @version  12.1
 * @since    1.0
 * 
 */    

/**
 * define constans rotuter, assets, fs
 */
const crossPromotionsRouter = require('express').Router();
const crossPromotions = require(__base + 'models/crossPromotions');
const assets = require(__base + 'models/assets');
const fs = require('fs');

// Create new crossPromotion item
/**
 *  routing - post to '/'
 *  
 * @param  req  - request
 * @param  {String} res - response param
 * @return  -   result or error
 */
crossPromotionsRouter.post('/', function (req, res) {
	// Check crossPromotion item data
    if (!req.body.crossPromotionTableId || !req.body.index || !req.body.order || !req.body.languages ||
        req.body.languages.length < 1 || (typeof req.body.showInPopup != 'boolean') ||
        (typeof req.body.showInTable != 'boolean' )) return res.status(400).json({err: 'Bad data'});
	// Check crossPromotion items for avoid duplicate crossPromotion item in DB. CrossPromotion items unique by index plus crossPromotionTableId
    crossPromotions.read({
        crossPromotionTableId: req.body.crossPromotionTableId,
        index: req.body.index
    }, function (err, result) {
        if (err) return res.status(err).json(result);
        if (result.length != 0) return res.status(400).json({err: 'Duplicate crossPromotion'});
		// If we not have duplicate crossPromotion item, then create new crossPromotion item
        crossPromotions.create(req.body, function (err, result) {
            if (err) return res.status(err).json(result);
            res.status(200).json(result);
        });
    });
});

/**
 * Get crossPromotion item by id
 * 
 * @param  req  - request
 * @param  {String} res - response param
 * @return  -   result or error
 */
crossPromotionsRouter.get('/(:id)?', function (req, res) {
    if (!req.params.id) return res.status(400).json({err: 'Bad params id'});
    crossPromotions.read({id: req.params.id}, function (err, selectCrossPromotionData) {
        if (err) return res.status(err).json(selectCrossPromotionData);
		// If crossPromotionId not have in BD, send error.
        if (selectCrossPromotionData.length != 1) return res.status(400).json({err: 'Bad crossPromotion id'});
		// Search asset for current crossPromotion
        assets.read({
            crossPromotionId: [req.params.id]
        }, null, false, function (err, selectAssetData) {
            if (err) return res.status(err).json({err: selectAssetData});
            if (selectAssetData.length != 1) return res.status(200).json(selectCrossPromotionData[0]);
			// Rename asset data
            selectCrossPromotionData[0].assetId = selectAssetData[0].id;
            selectCrossPromotionData[0].assetName = selectAssetData[0].name;
            res.status(200).json(selectCrossPromotionData[0]);
        });
    });
});

// Update crossPromotion item by id and crossPromotion new data

/**
 * Update crossPromotion item by id and crossPromotion new data
 * 
 * @param  req  - request
 * @param  {String} res - response param
 * @return  -   result or error
 */
crossPromotionsRouter.put('/(:id)?', function (req, res) {
    if (!req.params.id) return res.status(400).json({err: 'Bad params id'});
    crossPromotions.read({id: req.params.id}, function (err, selectCrossPromotionData) {
        if (err) return res.status(err).json(selectCrossPromotionData);
		// If crossPromotionId not have in BD, send error.
        if (selectCrossPromotionData.length != 1) return res.status(400).json({err: 'Bad crossPromotion id'});
	    // Check crossPromotion items for avoid duplicate crossPromotion item in DB. CrossPromotion items unique by index plus crossPromotionTableId
        if (req.body.index && req.body.index != selectCrossPromotionData[0].index) {
            crossPromotions.read({
                index: req.body.index,
                crossPromotionTableId: selectCrossPromotionData[0].crossPromotionTableId
            }, function (err, selectCrossPromotionDataFromIndex) {
                if (err) return res.status(err).json(selectCrossPromotionDataFromIndex);
                if (selectCrossPromotionDataFromIndex.length != 0) return res.status(400).json({err: 'Duplicate index'});
                updateCrossPromotion(selectCrossPromotionData);
            });
        } else updateCrossPromotion(selectCrossPromotionData);
    });

    function updateCrossPromotion(selectCrossPromotionData) {
		// Update crossPromotion without assset. Asset update in asset update request.
        crossPromotions.update(req.params.id, req.body, selectCrossPromotionData[0], function (err, updateCrossPromotionData) {
            if (err) return res.status(err).json(updateCrossPromotionData);
            res.status(200).json(updateCrossPromotionData);
        });
    }
});

/**
 * Delete crossPromotion item by id
 * 
 * @param  req  - request
 * @param  {String} res - response param
 * @return  -   result or error
 */
crossPromotionsRouter.delete('/(:id)?', function (req, res) {
    if (!req.params.id) return res.status(400).json({err: 'Bad params id'});
    crossPromotions.read({id: req.params.id}, function (err, readCrossPromotionResult) {
        if (err) return res.status(err).json(readCrossPromotionResult);
		// If crossPromotionId not have in BD, send error.
        if (readCrossPromotionResult.length == 0) return res.status(400).json({err: 'Bad crossPromotion id'});
		// Check asset for current crossPromotion. If crossPromotion have asset, temporary save link for asset file.
        assets.read({
            crossPromotionId: [req.params.id]
        }, null, false, function (err, selectAssetData) {
            if (err) return res.status(err).json({err: selectAssetData});
			// Delete crossPromotion. Asset delete with crossPromotion, because have foreighKey to crossPromotion.
            crossPromotions.delete({id: req.params.id}, function (err, deleteCrossPromotionResult) {
                if (err) return res.status(err).json(deleteCrossPromotionResult);
                if (selectAssetData.length != 1) return res.status(200).json(deleteCrossPromotionResult);
				// If crossPromotion have asset, delete asset file using link.
                selectAssetData.forEach(function (asset) {
                    try {
                        fs.unlinkSync(__base + 'static/' + asset.link);
                    }
                    catch (e) {
                        console.log(e);
                    }
                })
                return res.status(200).json(deleteCrossPromotionResult);
            });
        });
    });
});

module.exports.crossPromotionsRouter = crossPromotionsRouter;